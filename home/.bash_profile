alias ll='ls -la'
alias la='ls -a'

export PATH=~/bin:/usr/local/bin:/usr/local/sbin:$PATH
export PATH=$PATH:/usr/libexec

# JAVA
export JAVA_HOME=`java_home`

# android SDK
export ANDROID_HOME=~/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
